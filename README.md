# ESIT Training 
Lab 01: Training With Basic commands

---

## Preparations

 - Clean your docker host using the commands (in Powershell):
 
```
$ docker rm -f $(docker ps -a -q)
```

## Instructions

 - Run the application in a Docker container (detached mode) using:
```
$ docker run -d -p 3000:80 --name static-app nginx:latest
```

 - Now open the browser on you host and in the URL type :
```
localhost:3000 
*you will see Ngnix Welcome page
```

 - Check that the container is running:
```
$ docker ps
```

 - Check which images exist in your host:
```
$ docker images
```

 - Remove the running container (with the force flag):
```
$ docker rm -f static-app
```

 - Check the running containers:
```
$ docker ps
```
